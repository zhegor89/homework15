#include <iostream>

void FindNumbers(int limit, int mode)
{
    std::cout << 0 << std::endl;
    for (int i = mode; i <= limit; i += 2)
    {
        std::cout << i << std::endl;
    }
}

int main()
{
    int limit, mode;

    std::cout << "Enter limit: ";
    std::cin >> limit;
    std::cout << "Enter mode (1 - odd, 2 - even): ";
    std::cin >> mode;

    FindNumbers(limit, mode);    
}
